import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.rocksdb.*;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class SubscriptionCount {

    public static void main(String[] args) throws Exception {
        RocksDB.loadLibrary();
        String previousIntColumnFamily = "AcdStatusSnapshots";
        byte[] previousIntColumnFamilyBA = previousIntColumnFamily.getBytes(StandardCharsets.UTF_8);


        String nextIntcolumnFamily = "AgentStateSnapshots";
        byte[] nextIntcolumnFamilyBA = nextIntcolumnFamily.getBytes(StandardCharsets.UTF_8);

        String assignedInteractionscolumnFamily = "subscriptions";
        byte[] assignedInteractionsFamilyBA = assignedInteractionscolumnFamily.getBytes(StandardCharsets.UTF_8);

//        SecurityManager securityManager = new SecurityManager();
//
//        java.lang.System.setSecurityManager(new SecurityManager());

        try (final ColumnFamilyOptions cfOpts = new ColumnFamilyOptions().optimizeUniversalStyleCompaction()) {

            // list of column family descriptors, first entry must always be default column family
            final List<ColumnFamilyDescriptor> cfDescriptors = Arrays.asList(
                    new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY, cfOpts),
                    new ColumnFamilyDescriptor(previousIntColumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(nextIntcolumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(assignedInteractionsFamilyBA, cfOpts)


            );

            // a list which will hold the handles for the column families once the db is opened
            final List<ColumnFamilyHandle> columnFamilyHandleList = new ArrayList<>();


            String dbPath = args[0];

            try (final DBOptions options = new DBOptions()
                    .setCreateIfMissing(true)
                    .setCreateMissingColumnFamilies(true);

                 final RocksDB db = RocksDB.open(options, dbPath, cfDescriptors, columnFamilyHandleList)) {

                try {
                    for (ColumnFamilyHandle columnFamilyHandle : columnFamilyHandleList) {
                        byte[] name = columnFamilyHandle.getName();
//                        Class hashSet =Class.forName("java.util.HashSet");
                        System.out.println(new String(name));
                        TypeInformation<Long> resultType = TypeInformation.of(new TypeHint<Long>(){});
                        TypeSerializer<Long> serializer = resultType.createSerializer(new ExecutionConfig());
//                        TypeInformation<Set<String>> value = TypeInformation.of(new TypeHint<Set<String>>(){});
//                        TypeSerializer<Set<String>> listSerializer = value.createSerializer(new ExecutionConfig());

                        Map<String , Integer> susbcriptionCountPerDomain = new HashMap<>();

                        if((new String(name)).equals("subscriptions")) {
                            RocksIterator iterator = db.newIterator(columnFamilyHandle);
                            iterator.seekToFirst();
                            iterator.status();
                            DataInputDeserializer dataInputDeserializer = new DataInputDeserializer();
                            int count =0;

                            while (iterator.isValid()) {
                                byte[] key = iterator.key();
                                String subscriptionId= new String(key);
                                int ibeg = subscriptionId.indexOf("F");
                                String subscription = subscriptionId.substring(ibeg);
                                String[] Ids = subscription.split("-");
                                String domainId = Ids[2];

                                if(susbcriptionCountPerDomain.containsKey(domainId)){
                                    susbcriptionCountPerDomain.put(domainId ,susbcriptionCountPerDomain.get(domainId)+1 );
                                } else susbcriptionCountPerDomain.put(domainId , 1);
                                System.out.println(new String(key));
                                count++;
                                System.out.println("count --> "+ count);
//                                System.out.println(new String(iterator.value()));
                                iterator.next();
                            }
                            System.out.println(entriesSortedByValues(susbcriptionCountPerDomain));
                        }
                    }
                } finally {
                    // NOTE frees the column family handles before freeing the db
                    for (final ColumnFamilyHandle columnFamilyHandle :
                            columnFamilyHandleList) {
                        columnFamilyHandle.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static SortedSet<Map.Entry<String ,Integer>> entriesSortedByValues(Map<String ,Integer> map) {
        SortedSet<Map.Entry<String ,Integer>> sortedEntries = new TreeSet<Map.Entry<String ,Integer>>(
                new Comparator<Map.Entry<String ,Integer>>() {
                    @Override public int compare(Map.Entry<String ,Integer> e1, Map.Entry<String ,Integer> e2) {
                        int res = e1.getValue().compareTo(e2.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

}
