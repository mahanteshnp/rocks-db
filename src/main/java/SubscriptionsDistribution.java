import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.rocksdb.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SubscriptionsDistribution {

    static int count =0 ;
    static int slidingCount = 0;
    static int tumblingCount = 0;



    public static void main(String[] args) throws Exception {
        walkDirTreeAndSearch("/Users/mpatil/Downloads/results_latest/ATL/" , "");

    }


    private static void walkDirTreeAndSearch(String rootFolder, String search) throws Exception {
        Set<String> set = new HashSet<>();

        Map<String , Integer> map = new HashMap<>();
        Files.walk(Paths.get(rootFolder)).forEach(path -> {
            String operator  = path.toString();
           String filename =  path.getFileName().toString();
            if(operator.contains("_op_CoStreamFlatMap_")  && filename.equals("db")){
                    System.out.println("counting subscriptions --> " + path);
                getSubscriptionCount(path.toString() , map );
                }
        });
    }



    public static void getSubscriptionCount(String path , Map<String , Integer> susbcriptionCountPerDomain ){
                RocksDB.loadLibrary();
        String previousIntColumnFamily = "AcdStatusSnapshots";
        byte[] previousIntColumnFamilyBA = previousIntColumnFamily.getBytes(StandardCharsets.UTF_8);

        String longTermCache = "longTermCache";
        byte[] longTermCacheBA = longTermCache.getBytes(StandardCharsets.UTF_8);


        String reconciledInteractionsData = "reconciledInteractionsData";
        byte[] reconciledInteractionsDataBA = reconciledInteractionsData.getBytes(StandardCharsets.UTF_8);


        String interactionsInQueue = "interactionsInQueue";
        byte[] interactionsInQueueBA = interactionsInQueue.getBytes(StandardCharsets.UTF_8);

        String nextIntcolumnFamily = "AgentStateSnapshots";
        byte[] nextIntcolumnFamilyBA = nextIntcolumnFamily.getBytes(StandardCharsets.UTF_8);



        String mapSubscriptionsToHashKeys = "mapSubscriptionsToHashKeys";
        byte[] mapSubscriptionsToHashKeysBA = mapSubscriptionsToHashKeys.getBytes(StandardCharsets.UTF_8);

        String assignedInteractionscolumnFamily = "subscriptionsCacheFunction";
        byte[] assignedInteractionsFamilyBA = assignedInteractionscolumnFamily.getBytes(StandardCharsets.UTF_8);

        String lastProcessedIntervalStartTimestamp = "lastProcessedIntervalStartTimestamp";
        byte[] lastProcessedIntervalStartTimestampBA = lastProcessedIntervalStartTimestamp.getBytes(StandardCharsets.UTF_8);


        String lastUpdateTimestamp = "lastUpdateTimestamp";
        byte[] lastUpdateTimestampBA = lastUpdateTimestamp.getBytes(StandardCharsets.UTF_8);

        String mapHashKeysToSubscriptions = "mapHashKeysToSubscriptions";
        byte[] mapHashKeysToSubscriptionsBA = mapHashKeysToSubscriptions.getBytes(StandardCharsets.UTF_8);

        String shortTermCache = "shortTermCache";
        byte[] shortTermCacheBA = shortTermCache.getBytes(StandardCharsets.UTF_8);

        String lastEmittedValue = "lastEmittedValue";
        byte[] lastEmittedValueBA = lastEmittedValue.getBytes(StandardCharsets.UTF_8);

        String mapLongCacheKeyToShortCacheKeys = "mapLongCacheKeyToShortCacheKeys";
        byte[] mapLongCacheKeyToShortCacheKeysBA = mapLongCacheKeyToShortCacheKeys.getBytes(StandardCharsets.UTF_8);



//        SecurityManager securityManager = new SecurityManager();
//
//        java.lang.System.setSecurityManager(new SecurityManager());

        try (final ColumnFamilyOptions cfOpts = new ColumnFamilyOptions().optimizeUniversalStyleCompaction()) {

            // list of column family descriptors, first entry must always be default column family
            final List<ColumnFamilyDescriptor> cfDescriptors = Arrays.asList(
                    new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY, cfOpts),
                    new ColumnFamilyDescriptor(previousIntColumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(nextIntcolumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(interactionsInQueueBA, cfOpts),
                    new ColumnFamilyDescriptor(reconciledInteractionsDataBA, cfOpts),

                    new ColumnFamilyDescriptor(lastProcessedIntervalStartTimestampBA, cfOpts),
                    new ColumnFamilyDescriptor(mapLongCacheKeyToShortCacheKeysBA, cfOpts),
                    new ColumnFamilyDescriptor(lastEmittedValueBA, cfOpts),
                    new ColumnFamilyDescriptor(shortTermCacheBA, cfOpts),
                    new ColumnFamilyDescriptor(shortTermCacheBA, cfOpts),
                    new ColumnFamilyDescriptor(mapHashKeysToSubscriptionsBA, cfOpts),
                    new ColumnFamilyDescriptor(lastUpdateTimestampBA, cfOpts),
                    new ColumnFamilyDescriptor(mapSubscriptionsToHashKeysBA, cfOpts),
                    new ColumnFamilyDescriptor(longTermCacheBA, cfOpts),
                    new ColumnFamilyDescriptor(assignedInteractionsFamilyBA, cfOpts)


            );

            // a list which will hold the handles for the column families once the db is opened
            final List<ColumnFamilyHandle> columnFamilyHandleList = new ArrayList<>();


            String dbPath = path;
//            String dbPath = "/Users/mpatil/Downloads/foo/job_fd4c3596d33f686188141261db16c912_op_CoStreamFlatMap_b13113b37c571680e91b094b255c53ad__1_1__uuid_f84899c2-b273-4753-9c23-13be1c99e82f/db";

//            String dbPath ="/Users/mpatil/job_259a65ec1eedc204350cca3fc9180dd7_op_CoStreamFlatMap_b13113b37c571680e91b094b255c53ad__1_1__uuid_afee9ed3-1c30-4171-810e-940d98c09f15/db";
            try (final DBOptions options = new DBOptions()
                    .setCreateIfMissing(true)
                    .setCreateMissingColumnFamilies(true);

                 final RocksDB db = RocksDB.open(options, dbPath, cfDescriptors, columnFamilyHandleList)) {

                try {
                    for (ColumnFamilyHandle columnFamilyHandle : columnFamilyHandleList) {
                        byte[] name = columnFamilyHandle.getName();
//                        Class hashSet =Class.forName("java.util.HashSet");
                        System.out.println(new String(name));
                        TypeInformation<Long> resultType = TypeInformation.of(new TypeHint<Long>(){});
                        TypeSerializer<Long> serializer = resultType.createSerializer(new ExecutionConfig());
//                        TypeInformation<Set<String>> value = TypeInformation.of(new TypeHint<Set<String>>(){});
//                        TypeSerializer<Set<String>> listSerializer = value.createSerializer(new ExecutionConfig());

                        if((new String(name)).equals("subscriptionsCacheFunction")) {
                            RocksIterator iterator = db.newIterator(columnFamilyHandle);
                            iterator.seekToFirst();
                            iterator.status();
                            DataInputDeserializer dataInputDeserializer = new DataInputDeserializer();

                            while (iterator.isValid()) {
                                byte[] key = iterator.key();

                                String subscriptionId= new String(key);
                                int ibeg = subscriptionId.indexOf("F");
                                String subscription = subscriptionId.substring(ibeg);
                                String[] Ids = subscription.split("-");
                                Integer window = Integer.parseInt(Ids[3]);

                                if(window == 4){
                                    slidingCount ++;
//                                    System.out.println("slidingCount --> "+ slidingCount);
                                } else {
                                    tumblingCount ++;
//                                    System.out.println("tumblingCount --> "+ tumblingCount);
                                }

                                String domainId = Ids[0]+"_"+Ids[1]+"_"+Ids[2];

                                if(susbcriptionCountPerDomain.containsKey(domainId)){
                                    susbcriptionCountPerDomain.put(domainId ,susbcriptionCountPerDomain.get(domainId)+1 );
                                } else susbcriptionCountPerDomain.put(domainId , 1);
//                                System.out.println(new String(key));
                                count++;
//                                System.out.println("count --> "+ count);
//                                System.out.println(new String(iterator.value()));
                                iterator.next();
                            }
//                            System.out.println(entriesSortedByValues(susbcriptionCountPerDomain));
                        }
                    }
                    System.out.println("slidingCount --> "+ slidingCount);
                    System.out.println("tumblingCount --> "+ tumblingCount);
                    System.out.println("count --> "+ count);
                    writeMapToFile(entriesSortedByValues(susbcriptionCountPerDomain));
                    System.out.println("-------------------");
                } finally {
                    // NOTE frees the column family handles before freeing the db
                    for (final ColumnFamilyHandle columnFamilyHandle :
                            columnFamilyHandleList) {
                        columnFamilyHandle.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static SortedSet<Map.Entry<String ,Integer>> entriesSortedByValues(Map<String ,Integer> map) {
        SortedSet<Map.Entry<String ,Integer>> sortedEntries = new TreeSet<Map.Entry<String ,Integer>>(
                new Comparator<Map.Entry<String ,Integer>>() {
                    @Override public int compare(Map.Entry<String ,Integer> e1, Map.Entry<String ,Integer> e2) {
                        int res = e1.getValue().compareTo(e2.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    static void writeMapToFile(SortedSet<Map.Entry<String ,Integer>> result){
        File file = new File("/Users/mpatil/Downloads/results.txt");

        BufferedWriter bf = null;;

        try{

            //create new BufferedWriter for the output file
            bf = new BufferedWriter( new FileWriter(file) );

            Iterator<Map.Entry<String , Integer>> mapIterator = result.iterator();
            //iterate map entries
            while(mapIterator.hasNext()){
                Map.Entry<String , Integer> entry = mapIterator.next();
                bf.write( entry.getKey() + ":" + entry.getValue() );
                //new line
                bf.newLine();
            }


            bf.flush();

        }catch(IOException e){
            e.printStackTrace();
        }finally{

            try{
                //always close the writer
                bf.close();
            }catch(Exception e){}
        }
    }

}
