import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.rocksdb.*;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class TestRocksDB {
    public static void main(String[] args) throws Exception {
        RocksDB.loadLibrary();
        String previousIntColumnFamily = "InteractionEvent";
        byte[] previousIntColumnFamilyBA = previousIntColumnFamily.getBytes(StandardCharsets.UTF_8);

        String nextIntcolumnFamily = "FullAgentStateValue";
        byte[] nextIntcolumnFamilyBA = nextIntcolumnFamily.getBytes(StandardCharsets.UTF_8);

        String assignedInteractionscolumnFamily = "assignedInteractions";
        byte[] assignedInteractionsFamilyBA = assignedInteractionscolumnFamily.getBytes(StandardCharsets.UTF_8);

        try (final ColumnFamilyOptions cfOpts = new ColumnFamilyOptions().optimizeUniversalStyleCompaction()) {

            // list of column family descriptors, first entry must always be default column family
            final List<ColumnFamilyDescriptor> cfDescriptors = Arrays.asList(
                    new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY, cfOpts),
                    new ColumnFamilyDescriptor(previousIntColumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(nextIntcolumnFamilyBA, cfOpts),
                    new ColumnFamilyDescriptor(assignedInteractionsFamilyBA, cfOpts)
            );

            // a list which will hold the handles for the column families once the db is opened
            final List<ColumnFamilyHandle> columnFamilyHandleList = new ArrayList<>();
            String dbPath = "/Users/mpatil/Downloads/foo/job_fd4c3596d33f686188141261db16c912_op_CoStreamFlatMap_b13113b37c571680e91b094b255c53ad__1_1__uuid_f84899c2-b273-4753-9c23-13be1c99e82f/db";

            try (final DBOptions options = new DBOptions()
                    .setCreateIfMissing(true)
                    .setCreateMissingColumnFamilies(true);

                 final RocksDB db = RocksDB.open(options, dbPath, cfDescriptors, columnFamilyHandleList)) {

                try {
                    for (ColumnFamilyHandle columnFamilyHandle : columnFamilyHandleList) {
                        byte[] name = columnFamilyHandle.getName();
                        System.out.println(new String(name));
                        TypeInformation<Long> resultType = TypeInformation.of(new TypeHint<Long>(){});
                        TypeSerializer<Long> serializer = resultType.createSerializer(new ExecutionConfig());

                        if((new String(name)).equals("assignedInteractions")) {
                            RocksIterator iterator = db.newIterator(columnFamilyHandle);
                            iterator.seekToFirst();
                            iterator.status();
                            DataInputDeserializer dataInputDeserializer = new DataInputDeserializer();
                            while (iterator.isValid()) {
                                byte[] key = iterator.key();
                                dataInputDeserializer.setBuffer(key);
                                System.out.println(bytesToLong(key));
                                    byte[] res = iterator.value();
                                    System.out.println(new String(iterator.value()));
                                iterator.next();
                            }
                        }
                    }
                } finally {
                    // NOTE frees the column family handles before freeing the db
                    for (final ColumnFamilyHandle columnFamilyHandle :
                            columnFamilyHandleList) {
                        columnFamilyHandle.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes , 10 , 8);
        buffer.flip();//need flip
        return buffer.getLong();
    }
}
